import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { RoutesModule } from './app.routes.module';
import { InicioComponent } from './inicio/inicio.component';
import { AutenticacionModule  } from './autenticacion/autenticacion.module';
import { UsuarioListarComponent } from './usuario/usuario-listar/usuario-listar.component';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    InicioComponent,
    UsuarioListarComponent
  ],
  imports: [
    BrowserModule,
    RoutesModule,
    AutenticacionModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
