import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { environment } from "../../environments/environment";

@Injectable()

export class AutenticacionService{

    constructor(private readonly _httpClient: HttpClient){ }
    hola(){
        return 'Hola como estas'
    }

    login(correo: string, password: string){
        const credenciales = {
            correo,
            password
        }
        const respuestaLogin$ = this._httpClient
        .post(environment.url + '/autentificacion/login', credenciales)
        .subscribe(usuarioLegado => {
            console.log(usuarioLegado)
        },
        error=>{
            console.error(error)
        })

    // verificar el login

    }
}