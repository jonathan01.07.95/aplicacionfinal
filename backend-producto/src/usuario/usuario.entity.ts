import { Entity, Column, PrimaryGeneratedColumn } from "typeorm";
@Entity('usuario')

export class UsuarioEntity{
    @PrimaryGeneratedColumn()
    id: number

    @Column()
    nombre: string

    @Column()
    password: string

    @Column()
    correo: string
}