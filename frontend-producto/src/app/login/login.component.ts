import { Component, OnInit } from "@angular/core";
import { AutenticacionService } from "../autenticacion/autenticacion.service";

@Component({
    selector: 'login',
    templateUrl: './login.component.html' ,
    styleUrls: ['./login.component.css']
})

export class LoginComponent implements OnInit{
    constructor(private readonly _autenticacionService: AutenticacionService){}

    ngOnInit(){}
    
    login(){
        this._autenticacionService.login('jonathan@hotmail.com', 'abcJona1795')
    }
}