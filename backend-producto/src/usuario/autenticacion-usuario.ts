import { Controller, Post, Req, Body, BadRequestException } from "../../node_modules/@nestjs/common";
import { generarUsuarioLogin } from "./funciones/funciones.usuario";
import { UsuarioService } from "./usuario.service";
import { validate } from "class-validator";

const md5 = require('md5');

@Controller('autentificacion')

export class AutentificacionController {
    constructor(private readonly usuarioService: UsuarioService) {
    }

    @Post('login')
    async autentificacion(
        // @Body() UsuarioLoginDto
        @Body('correo') correo, //parametrosBody.usuario
        @Body('password') password //parametrosBody.password
    ) {
        const usuarioALogearse = generarUsuarioLogin(correo, password);
        const arregloErrores = await validate(usuarioALogearse) // arreglo []
        const existenErrores = arregloErrores.length > 0;

        if (existenErrores) {
            console.error('Errores: logeando al usuario - ', arregloErrores)
            throw new BadRequestException('Parametros incorrectos')
        } else {
            // login
            const usuarioEncontrado = await this.usuarioService.findByEmail(usuarioALogearse.correo);
            if (usuarioEncontrado) {
                const passwordCorrecto = (usuarioALogearse.password === usuarioEncontrado.password)
                console.log(usuarioALogearse.password)
                if (passwordCorrecto) {
                    return usuarioEncontrado;
                    // para redireccionar 
                } else {
                    console.error('Intento fallido de la contrasenia: no existe usuario', usuarioALogearse);
                    throw new BadRequestException('Error de password');
                }
            } else {
                console.error('Intento fallido: no existe usuario', usuarioALogearse);
                throw new BadRequestException('Error al login');
            }

        }
    }

}


