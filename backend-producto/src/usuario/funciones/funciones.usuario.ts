import { UsuarioLoginDto } from "../usuario-dto/usuario.dto";

export function generarUsuarioLogin(correo: string, password: string): UsuarioLoginDto {
    const usuarioLogin = new UsuarioLoginDto();
    usuarioLogin.correo = correo;
    usuarioLogin.password = password;
    return usuarioLogin;
}

export function generarUsuarioCrear(correo: string, password: string, nombre:string): UsuarioLoginDto {
    const usuarioACrear = new UsuarioLoginDto();
    usuarioACrear.correo = correo;
    usuarioACrear.password = password;
    usuarioACrear.nombre = nombre;
    return usuarioACrear;
}