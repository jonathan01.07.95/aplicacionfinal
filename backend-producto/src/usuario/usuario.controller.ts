import { Controller, Get, Param } from "@nestjs/common";
import { UsuarioService } from "./usuario.service";

@Controller('usuario')

export class UsuarioController {

    constructor(
        private readonly _usuarioService: UsuarioService
    ) {

    }
    @Get('buscarUno/:id')
    buscarUno(
        @Param('id') idUsuario
    ) {
        return this._usuarioService.buscarUno(idUsuario)
    }

    @Get('buscarTodos')
    buscarTodos() {
        return this._usuarioService.buscarTodos()
    }


}