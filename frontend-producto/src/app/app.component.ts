import { Component, OnInit } from '@angular/core';
import {AutenticacionService} from './autenticacion/autenticacion.service'
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {

  constructor(
    private readonly _autenticacion:AutenticacionService 
  ) {}


  ngOnInit(){
    console.log(this._autenticacion.hola())
  }

  title = 'Apliacion Final';
}
