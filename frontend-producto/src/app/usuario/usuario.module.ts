import { NgModule } from "@angular/core";
import { UsuarioListarComponent } from "./usuario-listar/usuario-listar.component";
import { HttpClientModule } from "@angular/common/http";
import { UsuarioService } from "./usuario.service"

@NgModule({
    declarations: [UsuarioListarComponent],
    providers: [UsuarioService],
    imports: [HttpClientModule]
})

export class UsuarioModule{

}