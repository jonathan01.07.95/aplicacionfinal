import { NgModule } from '@angular/core';
import { AutenticacionService } from './autenticacion.service';
import { EstaLogueadoGuard } from './esta-logeado.guard';
import { HttpClientModule } from '@angular/common/http';

@NgModule({
    declarations: [],
    imports: [ HttpClientModule],
    providers: [ AutenticacionService, EstaLogueadoGuard ],
})

export class AutenticacionModule {

}