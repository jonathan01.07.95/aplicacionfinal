import { Injectable } from "@nestjs/common";
import { InjectRepository } from "@nestjs/typeorm";
import { UsuarioEntity } from "./usuario.entity";
import { Repository, FindOneOptions } from "typeorm";

@Injectable()

export class UsuarioService{
    constructor(
        @InjectRepository(UsuarioEntity)
        private readonly _usuarioRepository: Repository<UsuarioEntity>
    ){   }   

    async buscarUno(id): Promise<UsuarioEntity> {
        return this._usuarioRepository.findOne(id)
    }

    async buscarTodos(): Promise<UsuarioEntity[]>{
        return this._usuarioRepository.find()
    }

    async crearUno(){

    }

    async findByEmail(correo: string): Promise<UsuarioEntity> {
        const opciones: FindOneOptions = {
            where: {
                correo
            }
        };
        const unUsuario = this._usuarioRepository.findOne(undefined, opciones)
        return unUsuario;
    }

}

