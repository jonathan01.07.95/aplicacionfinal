import { Routes, RouterModule} from '@angular/router';
import { NgModule } from '@angular/core';
import { LoginComponent } from './login/login.component';
import { InicioComponent } from './inicio/inicio.component';
import { EstaLogueadoGuard } from './autenticacion/esta-logeado.guard';
import { AutenticacionService } from './autenticacion/autenticacion.service';


const routes: Routes = [
    {
        path: 'login',
        component: LoginComponent
    },
    {
        path: 'inicio',
        component: InicioComponent,
        canActivate: [EstaLogueadoGuard]
    },
    {
        path: '',
        redirectTo: 'login',
        pathMatch: 'full'
    }

]

@NgModule({
    imports: [RouterModule.forRoot(routes,{useHash: true})],
    exports: [RouterModule]
})

export class RoutesModule{
    constructor(private readonly _autenticacionService: AutenticacionService){}
    

}