import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot } from '@angular/router';
import { AutenticacionService } from './autenticacion.service';

@Injectable()

export class EstaLogueadoGuard implements CanActivate {

    constructor(private readonly _autenticacionService: AutenticacionService) {

    }

    canActivate(parametroRuta: ActivatedRouteSnapshot) {
        return false
    }
}