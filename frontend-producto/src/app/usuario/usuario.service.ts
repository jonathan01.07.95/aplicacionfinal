import { environment } from "../../environments/environment";
import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";

@Injectable()

export class UsuarioService{
    
    constructor(private readonly _httpClient: HttpClient){  }

    buscarUsuarios(){
        const buscarTodos = this._httpClient.get(environment.url +'/usuario/obtenerTodos')
        .subscribe( respuesta=> {
            console.log(respuesta)
        })
    } 
}